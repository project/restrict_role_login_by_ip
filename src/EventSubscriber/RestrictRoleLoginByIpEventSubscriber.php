<?php

declare(strict_types=1);

namespace Drupal\restrict_role_login_by_ip\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event Subscriber RestrictRoleLoginByIpEventSubscriber.
 */
class RestrictRoleLoginByIpEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs the event subscriber object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Set message when user is denied access.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The kernel request event.
   */
  public function onKernelRequest(RequestEvent $event): void {
    $request = $event->getRequest();
    $queryLogout = $request->query->get('logout');
    if (!empty($queryLogout) && $queryLogout == 'restrict') {
      $this->messenger->addMessage($this->t('You are not allowed to login from this IP address. Please contact the Site Administrator.'), 'error', TRUE);
      $event->setResponse(new RedirectResponse(Url::fromRoute('user.login')->toString()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::REQUEST => 'onKernelRequest',
    ];
  }

}
